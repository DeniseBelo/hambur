import "./styles.css";

const Product = ({ item, handleClick }) => {
  return (
    <div className="product">
      <li>
        <h2>{item.name}</h2> <p>Categoria: {item.category} </p>
        <p> Preço: {item.price} </p>
        <button onClick={() => handleClick(item.id)}>
          Adicionar ao carrinho
        </button>
      </li>
    </div>
  );
};

export default Product;
