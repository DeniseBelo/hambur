import "./styles.css";
import Product from "../Product";

const Container = ({ products, handleClick }) => {
  return (
    <div>
      <ul className="container">
        {products.map((item, index) => (
          <Product key={index} item={item} handleClick={handleClick}></Product>
        ))}
      </ul>
    </div>
  );
};

export default Container;
