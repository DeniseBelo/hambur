import "./App.css";
import { useState } from "react";
import Container from "./components/Container";

const App = () => {
  const [products, setProducts] = useState([
    { id: 1, name: "Hamburguer", category: "Sanduíches", price: 7.99 },
    { id: 2, name: "X-Burguer", category: "Sanduíches", price: 8.99 },
    { id: 3, name: "X-Salada", category: "Sanduíches", price: 10.99 },
    { id: 4, name: "Big Kenzie", category: "Sanduíches", price: 16.99 },
    { id: 5, name: "Guaraná", category: "Bebidas", price: 4.99 },
    { id: 6, name: "Coca", category: "Bebidas", price: 4.99 },
    { id: 7, name: "Fanta", category: "Bebidas", price: 4.99 },
  ]);
  const [filteredProducts, setFilteredProducts] = useState([]);
  const [currentSale, setCurrentSale] = useState([]);
  const [cartTotal, setCartTotal] = useState(0);
  const [inputValue, setInputValue] = useState("");

  const showProducts = () => {
    setFilteredProducts(
      products.filter((item) => inputValue === item.name.toLowerCase())
    );
    setInputValue("");
  };

  const handleClick = (productId) => {
    const foundItem = products.find((item) => productId === item.id);
    if (!currentSale.includes(foundItem)) {
      setCurrentSale([...currentSale, foundItem]);
    }
    setFilteredProducts([]);
  };

  return (
    <div className="App">
      <h1> Mc Dedê Feliz!!! </h1>
      <div className="search">
        <input
          type="text"
          placeholder="Digite o produto aqui..."
          value={inputValue}
          onChange={(e) => setInputValue(e.target.value.toLowerCase())}
        ></input>
        <button onClick={showProducts}> Pesquisar </button>
      </div>

      <Container
        products={filteredProducts.length === 0 ? products : filteredProducts}
        handleClick={handleClick}
      ></Container>

      <div className="price">
        Subtotal: R$
        {currentSale.reduce((acc, item) => acc + item.price, 0).toFixed(2)}
      </div>
      <div className="cart">
        {currentSale.map((item, index) => (
          <div className="plus">
            <li className="additem" key={index}>
              <h2>{item.name}</h2>
              <p>Categoria: {item.category}</p>
              <p>Preço: R${item.price}</p>
            </li>
            <span className="span"> + </span>
          </div>
        ))}
      </div>
    </div>
  );
};

export default App;
